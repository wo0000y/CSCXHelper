#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //OutIO = new QRadioButton(5*24);

    //qDebug()<<"radioButton_Single autoExclusive："<< ui->radioButton_Single_1->autoExclusive();
    ui->radioButton_Single_1->setAutoExclusive(false);
    //qDebug()<<"radioButton_Single autoExclusive："<< ui->radioButton_Single_1->autoExclusive();
    QDIO_Init();
}

MainWindow::~MainWindow()
{
    //delete OutIO;

    delete [] OutBoardIOButtonGroup;
    delete [] OutBoardModeButtonGroup;

    delete ui;
}


void MainWindow::QDIO_Init(void)
{
    uint32_t i =0,j=0;
    OutBoardIOButtonGroup = new QButtonGroup[5];
    OutBoardModeButtonGroup = new QButtonGroup[5];

    /*将界面上的按钮存进二维数组*/
    QRadioButton *OutIOTemp[5][24] = {{ui->Button_OUT_1,ui->Button_OUT_2,ui->Button_OUT_3,ui->Button_OUT_4,
                                  ui->Button_OUT_5,ui->Button_OUT_6,ui->Button_OUT_7,ui->Button_OUT_8,
                                  ui->Button_OUT_9,ui->Button_OUT_10,ui->Button_OUT_11,ui->Button_OUT_12,
                                  ui->Button_OUT_13,ui->Button_OUT_14,ui->Button_OUT_15,ui->Button_OUT_16,
                                  ui->Button_OUT_17,ui->Button_OUT_18,ui->Button_OUT_19,ui->Button_OUT_20,
                                  ui->Button_OUT_21,ui->Button_OUT_22,ui->Button_OUT_23,ui->Button_OUT_24},

                                  {ui->Button_OUT_25,ui->Button_OUT_26,ui->Button_OUT_27,ui->Button_OUT_28,
                                   ui->Button_OUT_29,ui->Button_OUT_30,ui->Button_OUT_31,ui->Button_OUT_32,
                                   ui->Button_OUT_33,ui->Button_OUT_34,ui->Button_OUT_35,ui->Button_OUT_36,
                                   ui->Button_OUT_37,ui->Button_OUT_38,ui->Button_OUT_39,ui->Button_OUT_40,
                                   ui->Button_OUT_41,ui->Button_OUT_42,ui->Button_OUT_43,ui->Button_OUT_44,
                                   ui->Button_OUT_45,ui->Button_OUT_46,ui->Button_OUT_47,ui->Button_OUT_48},

                                  {ui->Button_OUT_49,ui->Button_OUT_50,ui->Button_OUT_51,ui->Button_OUT_52,
                                   ui->Button_OUT_53,ui->Button_OUT_54,ui->Button_OUT_55,ui->Button_OUT_56,
                                   ui->Button_OUT_57,ui->Button_OUT_58,ui->Button_OUT_59,ui->Button_OUT_60,
                                   ui->Button_OUT_61,ui->Button_OUT_62,ui->Button_OUT_63,ui->Button_OUT_64,
                                   ui->Button_OUT_65,ui->Button_OUT_66,ui->Button_OUT_67,ui->Button_OUT_68,
                                   ui->Button_OUT_69,ui->Button_OUT_70,ui->Button_OUT_71,ui->Button_OUT_72},

                                  {ui->Button_OUT_73,ui->Button_OUT_74,ui->Button_OUT_75,ui->Button_OUT_76,
                                   ui->Button_OUT_77,ui->Button_OUT_78,ui->Button_OUT_79,ui->Button_OUT_80,
                                   ui->Button_OUT_81,ui->Button_OUT_82,ui->Button_OUT_83,ui->Button_OUT_84,
                                   ui->Button_OUT_85,ui->Button_OUT_86,ui->Button_OUT_87,ui->Button_OUT_88,
                                   ui->Button_OUT_89,ui->Button_OUT_90,ui->Button_OUT_91,ui->Button_OUT_92,
                                   ui->Button_OUT_93,ui->Button_OUT_94,ui->Button_OUT_95,ui->Button_OUT_96},

                                 {ui->Button_OUT_97,ui->Button_OUT_98,ui->Button_OUT_99,ui->Button_OUT_100,
                                 ui->Button_OUT_101,ui->Button_OUT_102,ui->Button_OUT_103,ui->Button_OUT_104,
                                 ui->Button_OUT_105,ui->Button_OUT_106,ui->Button_OUT_107,ui->Button_OUT_108,
                                 ui->Button_OUT_109,ui->Button_OUT_110,ui->Button_OUT_111,ui->Button_OUT_112,
                                 ui->Button_OUT_113,ui->Button_OUT_114,ui->Button_OUT_115,ui->Button_OUT_116,
                                 ui->Button_OUT_117,ui->Button_OUT_118,ui->Button_OUT_119,ui->Button_OUT_120}};
    /*将IO按钮信息存进结构体数组和按钮组中*/
    for (i=0; i<5; i++)
    {
        for (j=0; j<24; j++)
        {
            qDebug()<<"OutBoardIOButtonGroup:" << OutIOTemp[i][j]->objectName();
            OutIO[i][j].BoardType = OutputBoard;
            OutIO[i][j].BoardNum = i;
            OutIO[i][j].id = j;
            OutIO[i][j].Objectaddr = OutIOTemp[i][j];
            OutBoardIOButtonGroup[i].addButton(OutIOTemp[i][j],j);
        }
        /*设置点击后连接到的槽函数*/
        connect(&OutBoardIOButtonGroup[i],SIGNAL(buttonToggled(QAbstractButton *, bool)),
                this, SLOT(QDIO_BeChecked(QAbstractButton *,bool)));
        /*设置唯一性，默认为true*/
        OutBoardIOButtonGroup[i].setExclusive(true);
    }

    /*将mode按钮信息存进结构体数组和按钮组中*/
    OutIO[0][24].Objectaddr = ui->radioButton_Single_1;
    OutIO[0][25].Objectaddr = ui->radioButton_NoneSingle_1;
    OutIO[0][26].Objectaddr = ui->radioButton_Water_1;

    OutIO[1][24].Objectaddr = ui->radioButton_Single_2;
    OutIO[1][25].Objectaddr = ui->radioButton_NoneSingle_2;
    OutIO[1][26].Objectaddr = ui->radioButton_Water_2;

    OutIO[2][24].Objectaddr = ui->radioButton_Single_3;
    OutIO[2][25].Objectaddr = ui->radioButton_NoneSingle_3;
    OutIO[2][26].Objectaddr = ui->radioButton_Water_3;

    OutIO[3][24].Objectaddr = ui->radioButton_Single_4;
    OutIO[3][25].Objectaddr = ui->radioButton_NoneSingle_4;
    OutIO[3][26].Objectaddr = ui->radioButton_Water_4;

    OutIO[4][24].Objectaddr = ui->radioButton_Single_6;/*此处驱动板5的控件命名时未按顺序*/
    OutIO[4][25].Objectaddr = ui->radioButton_NoneSingle_6;
    OutIO[4][26].Objectaddr = ui->radioButton_Water_6;

    for (i=0; i<5; i++)
    {
        for (j=24; j<27; j++)
        {
            OutIO[i][j].BoardType = OutputBoard;
            OutIO[i][j].BoardNum = 0;
            OutIO[i][j].id = j;

            OutBoardModeButtonGroup[i].addButton(OutIO[i][j].Objectaddr,j-24);
        }
        /*设置点击后连接到的槽函数*/
        connect(&OutBoardModeButtonGroup[i],SIGNAL(buttonToggled(QAbstractButton *, bool)),
                this, SLOT(QDIO_BeChecked(QAbstractButton *,bool)));
        /*设置唯一性，默认为true*/
        OutBoardModeButtonGroup[i].setExclusive(true);
    }
}

void MainWindow::QDIO_BeChecked(QAbstractButton *Button,bool state)
{
    uint32_t i = 0,j = 0;

    qDebug()<<"QDIO_"<< Button->objectName() << "= " << state;

    for (i=0; i<(5); i++)
    {
        for (j=0; j<27; j++)
        {
            if(OutIO[i][j].Objectaddr == Button)
            {
                if(24 > j)
                {
                    if( true == state)
                    {
                        Button->setStyleSheet("color: red");
                    }
                    else
                    {
                        Button->setStyleSheet("color: blank");
                    }
                }
                else
                {

                    if( (25 == j) && ( true == state) )/*多选模式*/
                    {
                        OutBoardIOButtonGroup[i].setExclusive(false);

                        qDebug()<<"2:OutIO[i][j]:" << i << "__" << j;

                    }
                    else if( (26 == j) && ( true == state) ) /*流水灯选模式*/
                    {
                        OutBoardIOButtonGroup[i].setExclusive(false);
                        OutIO[i][0].Objectaddr->setChecked(true);
                        OutIO[i][2].Objectaddr->setChecked(true);
                        qDebug()<<"3:OutIO[i][j]:" << i << "__" << j;
                    }
                    else/*单选模式*/
                    {
                        OutBoardIOButtonGroup[i].setExclusive(true);
                        qDebug()<<"1:OutIO[i][j]:" << i << "__" << j;
                    }
                }
            }
        }
    }
}

void MainWindow::GetQDIO_Data(void)
{
    uint32_t i = 0,j = 0;
    uint32_t OutIOData[5] = {0};

    for (i=0; i<(5); i++)
    {
        OutIOData[i] = 0;
        for (j=0; j<24; j++)
        {
            if( OutIO[i][j].Objectaddr->isChecked() )
            {
                OutIOData[i] |= (1 << j);
            }
        }
    }
}

