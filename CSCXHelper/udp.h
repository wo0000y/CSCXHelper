#ifndef UDP_H
#define UDP_H

#include <QObject>
#include <QtNetwork>
#include <QTimer>
#include <QNetworkDatagram>
#include <QWidget>

class UDP : public QUdpSocket
{
    Q_OBJECT
public:
    UDP(QUdpSocket *parent = nullptr);
    ~UDP();

    QByteArray UDPReceive;
    QList<QHostAddress> LocalIPAddress;

private:
    //QUdpSocket *udpSocket;
    QTimer timer;

    void readPendingDatagrams();
};

#endif // UDP_H
