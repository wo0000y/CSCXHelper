#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include <QButtonGroup>


/*板卡类型*/
typedef enum {
    InputBoard,
    OutputBoard
}Enum_BoardType;

/*IO点位结构体*/
typedef struct {
    Enum_BoardType BoardType;
    uint8_t BoardNum;
    uint8_t id;
    QRadioButton *Objectaddr;
}Struct_OutIOIfo;



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

     Ui::MainWindow *ui;
private:

    void QDIO_Init(void);
    void QDIO_ModeSet(uint8_t board);




    QButtonGroup *OutBoardIOButtonGroup;
    QButtonGroup *OutBoardModeButtonGroup;
    //QRadioButton *(*OutIO)[5][24];

    Struct_OutIOIfo OutIO[5][27];/*24个开关量 + 3个模式设置按钮*/

protected slots:    //使用slots关键字
   void QDIO_BeChecked(QAbstractButton *,bool);
   void GetQDIO_Data(void);
};
#endif // MAINWINDOW_H
