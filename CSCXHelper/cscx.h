#ifndef CSCX_H
#define CSCX_H

#include <QObject>
#include "udp.h"
#include "mainwindow.h"

class CSCX :public UDP
{
    Q_OBJECT
public:
    CSCX(MainWindow *MainWindow);
    ~CSCX();

    void ScanIP(void);
    QHostAddress myaddress;
    qint32 myport;
    MainWindow *myMainWindow;
    QPushButton *ConnetButton;

public slots:
    void connect(void);



};

#endif // CSCX_H
