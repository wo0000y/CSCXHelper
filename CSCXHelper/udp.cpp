#include "udp.h"
#include <qdebug.h>
#include <QHostAddress>

UDP::UDP(QUdpSocket *parent)
    : QUdpSocket(parent)
{
    //udpSocket = new QUdpSocket();
    QHostAddress cscx_IPaddress;

    qDebug()<<"-------------------------------------------------------------------";
    //获取主机名
    QString localHostName = QHostInfo::localHostName();
    qDebug() << "localHostName" << localHostName;

    //获取本机的IP地址(可能有多条）
    QHostInfo info = QHostInfo::fromName(localHostName);
    qDebug()<<"IP Address："<< info.addresses();

    //只输出IPv4地址（可能有多条）
    foreach(QHostAddress address,info.addresses())
    {
        if(address.protocol() == QAbstractSocket::IPv4Protocol)

            qDebug() <<"My localhost IPv4 address: "<< address.toString();
        if("192.168.60.108" == address.toString())
        {
            cscx_IPaddress = address;
            qDebug() <<"cscx_IPaddress: "<< cscx_IPaddress.toString();
        }
    }
    qDebug()<<"-------------------------------------------------------------------";
    LocalIPAddress = info.addresses();
    //只输出IPv4地址（可能有多条）
    foreach(QHostAddress address,LocalIPAddress)
    {
        if(address.protocol() == QAbstractSocket::IPv4Protocol)

            qDebug() <<"My localhost IPv4 address: "<< address.toString();
        if("192.168.60.108" == address.toString())
        {
            cscx_IPaddress = address;
            qDebug() <<"cscx_IPaddress: "<< cscx_IPaddress.toString();
        }
    }

    qDebug()<<"-------------------------------------------------------------------";



    //bind(cscx_IPaddress, 7755);
    QObject::connect(this, &QUdpSocket::readyRead,
                  this, &UDP::readPendingDatagrams);
}

UDP::~UDP()
{
    //delete udpSocket;
}

void UDP::readPendingDatagrams()
{
    while (hasPendingDatagrams()) {
        QNetworkDatagram datagram = receiveDatagram();
        qDebug() << datagram.senderAddress() ;
        qDebug() << datagram.data() ;

        UDPReceive.append(datagram.data());

        writeDatagram(UDPReceive.data(),
                      UDPReceive.size(),
                      datagram.senderAddress(),
                      datagram.senderPort());
    }
}




